import axios from 'axios';
import React, { useState } from 'react';

function CreateForm({ fetchUsers }) {
	const [input, setInput] = useState({
		name: '',
		username: '',
		email: '',
		domisili: '',
		alamat: '',
	});

	const handleChange = e => {
		if (e.target.name === 'name') {
			setInput({ ...input, name: e.target.value });
		} else if (e.target.name === 'username') {
			setInput({ ...input, username: e.target.value });
		} else if (e.target.name === 'email') {
			setInput({ ...input, email: e.target.value });
		} else if (e.target.name === 'domisili') {
			setInput({ ...input, domisili: e.target.value });
		} else if (e.target.name === 'alamat') {
			setInput({ ...input, alamat: e.target.value });
		}
	};

	const handleSubmit = async () => {
		try {
			const response = await axios.post('http://arhandev.xyz/public/api/users', {
				name: input.name,
				username: input.username,
				email: input.email,
				domisili: input.domisili,
				alamat: input.alamat,
			});
			fetchUsers();
			setInput({
				name: '',
				username: '',
				email: '',
				domisili: '',
				alamat: '',
			});
		} catch (error) {
			alert(error.response.data.info);
		}
	};
	return (
		<div className="form">
			<h2 style={{ textAlign: 'center' }}>Create Form</h2>
			<form className="form-input">
				<div className="form-input-group">
					<label>Nama:</label>
					<input type="text" onChange={handleChange} placeholder="Masukkan nama" name="name" value={input.name} />
				</div>
				<div className="form-input-group">
					<label>Username:</label>
					<input
						type="text"
						onChange={handleChange}
						placeholder="Masukkan username"
						name="username"
						value={input.username}
					/>
				</div>
				<div className="form-input-group">
					<label>Email:</label>
					<input type="text" onChange={handleChange} placeholder="Masukkan email" name="email" value={input.email} />
				</div>
				<div className="form-input-group">
					<label>Domisili:</label>
					<input
						type="text"
						onChange={handleChange}
						placeholder="Masukkan domisili"
						name="domisili"
						value={input.domisili}
					/>
				</div>
				<div className="form-input-group">
					<label>Alamat:</label>
					<input type="text" onChange={handleChange} placeholder="Masukkan nama" name="alamat" value={input.alamat} />
				</div>
				<div className="button-container">
					<button onClick={handleSubmit} type="button">
						Simpan
					</button>
				</div>
			</form>
		</div>
	);
}

export default CreateForm;
