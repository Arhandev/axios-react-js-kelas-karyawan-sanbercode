import axios from 'axios';
import { useEffect, useState } from 'react';
import './App.css';
import CreateForm from './components/CreateForm';
import EditForm from './components/EditForm';

function App() {
	const [users, setUsers] = useState([]);
	const [edit, setEdit] = useState({});

	const fetchUsers = async () => {
		try {
			const response = await axios.get('http://arhandev.xyz/public/api/users');
			setUsers(response.data.data);
		} catch (error) {
			console.log(error);
		}
	};

	const deleteUser = async userId => {
		try {
			const response = await axios.delete(`http://arhandev.xyz/public/api/users/${userId}`);
			fetchUsers();
		} catch (error) {
			console.log(error);
		}
	};

	useEffect(() => {
		fetchUsers();
	}, []);
	return (
		<div className="App">
			{Object.keys(edit).length === 0 ? (
				<CreateForm fetchUsers={fetchUsers} />
			) : (
				<EditForm fetchUsers={fetchUsers} edit={edit} setEdit={setEdit} />
			)}
			<table>
				<thead>
					<tr>
						<th>Nama</th>
						<th>Username</th>
						<th>Email</th>
						<th>Domisili</th>
						<th>Alamat</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{users.map((user, index) => (
						<tr key={index}>
							<td>{user.name}</td>
							<td>{user.username}</td>
							<td>{user.email}</td>
							<td>{user.domisili}</td>
							<td>{user.alamat}</td>
							<td>
								<div className="action">
									<button onClick={() => setEdit(user)} className="btn btn-edit">
										Edit
									</button>
									<button onClick={() => deleteUser(user.id)} className="btn btn-delete">
										Delete
									</button>
								</div>
							</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>
	);
}

export default App;
